#!/usr/bin/python
# create SCAN_MATRIX blob for synaptic 06cb:009a finger print sensor
#
from validitysensor.sensor import *
from validitysensor.hw_tables import *

header_path = './scan_matrix.h'

def write_header(x):
    mylist = ('''/*
 * generated 00x9a scan_matrix header file from sensor calib file
 *
 * Validity VFS0090 driver for libfprint
 * Copyright (C) 2017 Nikita Mikhailov <nikita.s.mikhailov@gmail.com>
 * Copyright (C) 2018-2019 Marco Trevisan <marco@ubuntu.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#pragma once

unsigned char SCAN_MATRIX[] = {''')
    num=1
    mylist +='\n  '
    for i in x:
        if num <= 8:
            mylist=mylist + f"0x{i:02x}" + ", "
            num=num + 1
        else:
            mylist += '\n'
            mylist +='  ' + f"0x{i:02x}" + ", "
            num=2
    # trim last comma
    mylist = mylist[:-2] + '\n};\n'
    return mylist

if os.path.isfile(header_path):
    if os.path.getctime(header_path) > os.path.getctime(calib_data_path):
        exit(1)

# load calib file
if os.path.isfile(calib_data_path):
    with open(calib_data_path, 'rb') as f:
        sensor.calib_data = f.read()
        logging.info('Calibration data loaded from a file.')

# blobs getting
sensor.hardcoded_prog = unhex(''' 
23000000200008000020008000000100320074000000008020200400242000005020773628200100
302001003c208000082138000c210000482107004c210000582000005c2000006020000068200500
6c20014970200141742001887820018084202000942001809c200902a0200b19b4200300b8203b04
bc201400c0200200c4200100c82002003300100000000080cc200000f503d0200000a10132004400
00000080dc20e803e0206401e420d002e8200001f0200500f8200500fc200000b8203a0000080400
1408000008080000080800001408300008080000140831001c081a0032000c000000008050110100
4c111e00340078010000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000
10221710221710221610221610221601065010250101000007c8078c06ff0000000000004f80006d
0300280307030990098db00b90880991858e08c1810b9190910ac1b8928a0993878a890b93888989
08c88191890ac8889289099a818a890b9a88898908d08191890ad08892890802818a095a810a0288
890b5a8808d98189890ad9908989095e8289890b5e88898908e18189890ae190898909648289890b
648889096e8108e981890b6e880ae99091b9096f828a8f0b6f88918908f0818a890af09089890976
8289910b76b8918a08f88792910af8888a92097c81898a0b7c09018089890b01888991097f818992
0b7f09088089920b088889920c07030307200402000000002f000400700000002900040070000000
3500040080000000
''')

sensor.get_factory_bits = unhex('''
00001c0c0000060000000800000074000e0003000080070000007d7d7c777e7b757b817e7e7b7685
7f87878385898888898b8b8b8c8e8d87898d888a89888b8b8687867e8b8c838a8a8d8f8b8f908f86
858c8a8d8f908a848f8694918b8e918d8e8d898e8e8c8e8f8e8d898987818b8b828787848288898e
8f8081888a8b8b878183797a7e7c757a80818400000034020e000c00008007000000f7f7f9fffffe
fbfcfdfcfe0005090d0e0a0401010300fefe010403010001fffdfafafbfcfdfcfdff010203060501
faf6f2f6f9fbf9f8f5f4f5fbfffffcfafbfdfffefbfcfdfdfaf9fcff0100020103050a0b0a050303
0201fcf9f7f7f5f2f3f2f3f1f1f3f7fbfcff0000faf5f3f4f6f8ecf3f6f7f9fbfaf8f4eeedf1f6f8
f5f6f3f4f4f7f7f4f3f4f8fb0004060501fffe0203020101090e12131311110e0b0805050100020e
14140b060100fefaf8f7fa00020100fffef9f7f6fafd00fefeff00fffefcfdfefefdfdfafcfdfef9
f7f7fbfaf7f6f8fdfcfcfe030400fbf6f6f7f2f6f7f7f7f9fafaf7f1efececebedf2f3f4f5f5f3f3
f4f6f5f6f5f7f7fafafafafbfafafd01070c0f0c0a0806010001030401020407080808080a0a0b07
07080a080402ff00fffdf9f8fafdfefffe0100fcf6f6f4f2f6fa000101fefdfcf9f8f4f2f1f2f5fa
fbfcfe0104080c0e0b06e1f2f4f4f5f8f8f7f4f7f7f9f6f4f2f2f5f9f8f6f1eeedf1f3f4f4f7f9ff
050504020505060707050406060b1012100f1012171c1f1d1613110f0c08090b0b0b0c0d0e0e0e0f
0d0c0905030201fefdfcfcfcfcfaf6f6fa010404ff010102fdfcfbfaf9f8fafc0104050603030404
0603f4fcfefdff04090704fcf8f6fbfd00fffefdff03080a0805030405090a0a0704fefefaf9f7f7
f9fbfdfdfeffff0100fefcfcfafafd0204030100fffdfcfafcfd00050b11100e07fffcfd01020506
070a0804fefcfafaf8f7f9fe02fdfbfafcf9f9faff06090b0908050404080b0e100cc00200001400
0e000f000080055500078902210005870007810100200706e207dc02000008000e00080000809901
000000000000ec02000008000e0002000000000000005a009001fc02000004000e00050000802861
0200
''')

sensor.factory_calibration_values = unhex('''
7d7d7c777e7b757b817e7e7b76857f87878385898888898b8b8b8c8e8d87898d888a89888b8b8687
867e8b8c838a8a8d8f8b8f908f86858c8a8d8f908a848f8694918b8e918d8e8d898e8e8c8e8f8e8d
898987818b8b828787848288898e8f8081888a8b8b878183797a7e7c757a8081
''')

sensor.device_info = DeviceInfo(0x0190, 0x0199, 0x5a, 0xff, '57K0 FM-3367-003   ')
sensor.type_info = SensorTypeInfo.get_by_type(sensor.device_info.type)
sensor.rom_info = RomInfo(timestamp=1415491824, build=164, major=6, minor=7, product=48, u1=0)
# hardcoded for sensor type 0x199
sensor.key_calibration_line = 0x38
sensor.calibration_frames = 3 # TODO: workout where it's really comming from
sensor.calibration_iterations = 3 # hardcoded for type

lines_2d = [unpack('<L', v)[0] for [k, v] in prg.split_chunks(sensor.hardcoded_prog) if k == 0x2f][0]

sensor.lines_per_frame = lines_2d*sensor.type_info.repeat_multiplier
sensor.bytes_per_line = sensor.type_info.bytes_per_line

matrix = sensor.build_cmd_02(CaptureMode.IDENTIFY)

with open(header_path, 'w') as f:
    f.write(write_header(matrix))
    logging.info('%s created', header_path )
