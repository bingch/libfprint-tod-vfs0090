## Synaptics Sensor `06cb:009a` libfprint driver
#### A linux driver for 2016 ThinkPad's fingerprint readers
Tested on Lenovo Carbon X1 Gen 6 and T480s 

A fork of Marco's tod driver (https://gitlab.freedesktop.org/3v1n0/libfprint-tod-vfs0090) with blobs and minor adjustment to make it work for Synaptics Sensor `06cb:009a`. It only works for vefifying user, and depend on python-validity (see below)

### Device initialization and pairing and enroll finger on chip

I use uunicorn's python-validity(https://github.com/uunicorn/python-validity) to initialize, pair and enroll finger on chip using tools coming with python-validity's playground, make sure you pair the fingerprint then enroll at least one finger BEFORE you install this driver, as ths driver need the calib file generated by sensor initialization to compile properly and currently don't have the ability to actually enroll fingerprint on the chip yet, here's the steps to pair and enroll fingerprint using scripts in playground folder of uunicorn's python-validity  
<pre>
/usr/share/python-validity/playground $ sudo python factory-reset.py # reset & pair fingerprint
/usr/share/python-validity/playground $ sudo python
Python 3.7.8 (default, Jul 23 2020, 19:32:39) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from prototype import *
>>> open9x()
>>> enroll(sid_from_string('S-1-5-21-123456789-1111111456-1111111123-1000'), 0xf5)
...
Created a finger record with dbid 6
>>> db.dump_all()
 5: User S-1-5-21-123456789-1111111456-1111111123-1000 with 1 fingers:
     6: f5 (WINBIO_FINGER_UNSPECIFIED_POS_01)
>>> identify()
Got finger f5 for user recordid 5. Hash: 3d2451cbd9c590b0259f6775754937648268274bbdd45f56d687e75152abb5d1
>>> 
</pre>
### Setup user to use fingerprint
1. Install the TOD branch of Marco's libfprint repo https://gitlab.freedesktop.org/3v1n0/libfprint.git
<pre>
git clone https://gitlab.freedesktop.org/3v1n0/libfprint.git
cd libfprint/
git checkout tod
meson setup --prefix=/usr BUILD
ninja -C BUILD/
sudo meson install -C BUILD
</pre>
2. Install this driver https://gitlab.com/bingch/libfprint-tod-vfs0090.git
<pre>
git clone https://gitlab.com/bingch/libfprint-tod-vfs0090.git
cd libfprint-tod-vfs0090/
meson setup BUILD
ninja -C BUILD/
sudo meson install -C BUILD
</pre>
3. Install latest fprintd (> 1.90) from https://gitlab.freedesktop.org/libfprint/fprintd.git
<pre>
git clone https://gitlab.freedesktop.org/libfprint/fprintd.git
cd fprintd
meson setup --prefix=/usr BUILD
ninja -C BUILD/
sudo meson install -C BUILD
</pre>
4. Use fprint-enroll or gnome setting to setup user to use finger print to authenticate:
<pre>
/usr/share/python-validity/playground $ fprintd-enroll $USER
sing device /net/reactivated/Fprint/Device/0
Enrolling right-index-finger finger.
Enroll result: enroll-completed
/usr/share/python-validity/playground $ fprintd-verify $USER
Using device /net/reactivated/Fprint/Device/0
Listing enrolled fingers:
 - #0: right-index-finger
Verify result: verify-match (done)

</pre>
### Gentoo overlay

I've got ebuilds for the above software at https://gitlab.com/bingch/gentoo-overlay.git for gentoo user.
